import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  loginUserData = {}
  constructor(private _auth: AuthService,private _router: Router) { }

  ngOnInit() {
  }
  loginUser(){
    console.log(this.loginUserData)
    this._auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
        console.log(res)
        localStorage.setItem('jwt',res.token)
        this._router.navigate(['/pages'])
      },
      err =>{ console.log(err),
        this._router.navigate(['auth/login'])
      })
   
    

}
registerUser(){
  this._router.navigate(['auth/register'])
}
  
}
