import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _loginUrl = "http://localhost:3002/api/login";
  private _registerUrl = "http://localhost:3002/api/register";
  private _eventsUrl = "http://localhost:3002/api/events";
  constructor(private http: HttpClient) { }
  loginUser(user) {
    return this.http.post<any>(this._loginUrl, user)
  }
  registerUser(user) {
    return this.http.post<any>(this._registerUrl, user)
  }
  loggedIn() {
    console.log(!!localStorage.getItem('jwt'))
    return !!localStorage.getItem('jwt')    
  }  
  getToken() {
    return localStorage.getItem('jwt')
  }
  
  veryfye() {
    return this.http.get<any>(this._eventsUrl)
  }

}
