import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerUserData = {}
  constructor(private _auth: AuthService,private _router: Router) { }

  ngOnInit() {
  }
  registerUser() {
    console.log(this.registerUserData)
    this._auth.registerUser(this.registerUserData)
    .subscribe(
      res => {
        console.log(res)
        localStorage.setItem('jwt',res.token)
        this._router.navigate(['/login'])
      },
      err =>{ console.log(err),
        this._router.navigate(['/register'])
      })
    
  }
  loginUser(){
    this._router.navigate(['auth/login'])
  }
}
